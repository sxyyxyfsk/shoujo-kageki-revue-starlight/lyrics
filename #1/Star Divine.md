# Star Divine
* **作词：** 中村彼方
* **作曲、编曲：** 本多友紀（Arte Refact）
* **演唱：** スタァライト九九組
  * [<img src="../static/badge-karen_aijo.svg" height="20px" width="20px" style="vertical-align:middle" alt="爱城华恋"/>](https://zh.moegirl.org/爱城华恋) 愛城華恋
  * [<img src="../static/badge-maya_tendo.svg" height="20px" width="20px" style="vertical-align:middle" alt="天堂真矢"/>](https://zh.moegirl.org/天堂真矢) 天堂真矢
  * [<img src="../static/badge-hikari_kagura.svg" height="20px" width="20px" style="vertical-align:middle" alt="神乐光"/>](https://zh.moegirl.org/神乐光) 神楽ひかり
  * [<img src="../static/badge-junna_hoshimi.svg" height="20px" width="20px" style="vertical-align:middle" alt="星见纯那"/>](https://zh.moegirl.org/星见纯那) 星見純那
  * [<img src="../static/badge-mahiru_tsuyuzaki.svg" height="20px" width="20px" style="vertical-align:middle" alt="露崎真昼"/>](https://zh.moegirl.org/露崎真昼) 露崎まひる
  * [<img src="../static/badge-nana_daiba.svg" height="20px" width="20px" style="vertical-align:middle" alt="大场奈奈"/>](https://zh.moegirl.org/大场奈奈) 大場なな
  * [<img src="../static/badge-claudine_saijo.svg" height="20px" width="20px" style="vertical-align:middle" alt="西条克洛迪娜"/>](https://zh.moegirl.org/西条克洛迪娜) 西條クロディーヌ
  * [<img src="../static/badge-futaba_isurugi.svg" height="20px" width="20px" style="vertical-align:middle" alt="石动双叶"/>](https://zh.moegirl.org/石动双叶) 石動双葉
  * [<img src="../static/badge-kaoruko_hanayagi.svg" height="20px" width="20px" style="vertical-align:middle" alt="花柳香子"/>](https://zh.moegirl.org/花柳香子) 花柳香子

## 歌词

### 日文

```
躊躇う暇はない 回り出した歯車
覚悟と言う名の剣を さあ掲げて
結末も知らずに レヴューの幕が開けた
私は私を 再び造りあげる
心染めた願いをそっと呟いた時
瞬くは 導きの星

輝くの Star Divine
生まれたての光で
切り開け Star Divine
譲れない未来へ
切っ先に栄光止まれ

どこへ通じるかは 舞台のみぞ知ること
約束結んだ糸が 今に千切れそう
乙女の煌きが この歴史を動かす
たぎった情熱 ほとばしるまま進め
苦い風が吹いたのは心揺らいだせいね
私たち試されてるの

負けないで Star Divine
未来を見捨てないで
立ち上がれ Star Divine
何度傷ついても
舞台に生かされている

宇宙の砂になった 遠い空で繋ぐ星座
それは運命 それは本能
今もう一度 私になっていくから

輝くの Star Divine
生まれたての光で
切り開け Star Divine
譲れない未来へ

負けないで Star Divine
未来を見捨てないで
立ち上がれ Star Divine
何度傷ついても
舞台に生かされている
切っ先に栄光止まれ
```

### 中文

>歌词翻译由**圣翔音乐学院放送科**提供，**使用前请联系以获得转载许可**：
>* 微博 [@RevueStarlight](https://weibo.com/RevueStarlight)
>* bilibili [@圣翔音乐学院放送科](https://space.bilibili.com/234441166)

```
开始转动的齿轮无暇踌躇
来吧 举起名为觉悟之剑
尚未知晓结局之时 舞台的幕布已然揭开
我再次重铸自我
悄声低语着浸染心灵的愿望时
不断闪烁的是那指引的星

闪耀的 Star Divine 
用初生的光芒
开拓吧 Star Divine
向着那决不退让的未来
让荣光落于刀锋之上

只有舞台才知晓未来将通往何方
缔结约定之线 如今已快被撕碎
少女的光辉 撼动着这个历史
迸发着沸腾般的热情不断前进
会吹过苦涩的风是因为内心动摇了吧
我们正历经考验

不要认输 Star Divine
不要舍弃未来
振奋起来 Star Divine
无论受伤了多少次
舞台赋予了我们生存的意义

在遥远天空紧密相连的星座 已变为了宇宙中的砂砾
那就是宿命 那就是本能
如今我将再一次变回我自己

闪耀的 Star Divine
用那初生的光芒
开拓吧 Star Divine
向着那决不退让的未来

不要认输 Star Divine
不要舍弃未来
振奋起来 Star Divine
无论受伤了多少次
舞台赋予了我们生存的意义
让荣光落于刀锋之上
```